
let day1 = {
    squirel: false,
    events: ["work", "touched tree", "pizza", "running"]
};
day1.squirel = true;
day1.events[1] = "sat on tree trunk";
console.log(day1.squirel)

console.log(day1.events[day1.events.length - 1])

let descriptions = {
    'work': "Went to Work",
    'touched_tree': "touched a tree"
}

console.log(Object.assign(day1, descriptions))

Object.create(descriptions)

console.log(typeof[])

let journ = [
    {
        events: ["work", "touched tree", "pizza", "running", " television"],
        squirel: false
    },
    {
        events: ["work", "ice cream", "cauliflower", "lasagna", "touched tree", "brushed teeth"],
        squirel: false
    },
    {
        events: ["weekend", "Cycling", "break", "peanuts", "beer"],
        squirel: true
    }
];

console.log(journ)

let object1 = {value: 100};
let object2 = {value: 100};
let object3 = object1;


console.log(object1 === object2) // -> false : these are two different objects in heap having different pointers
console.log(object1 === object3) // -> true : they share the same pointer i.e they are interned


let journal  = []

function addEntry(events, squirrel) {
    journal.push({events, squirrel});
}

addEntry(["work", "touched tree", "pizza", "running",
"television"], false);
addEntry(["work", "ice cream", "cauliflower", "lasagna",
"touched tree", "brushed teeth"], false);
addEntry(["weekend", "cycling", "break", "peanuts",
"beer"], true);


console.log(journal)

/**
 * @param {any[]} table
 */
function phi(table) {
    return (table[3] * table[0] - table[2] * table[1]) /
    Math.sqrt((table[2] + table[3]) *
    (table[0] + table[1]) *
    (table[1] + table[3]) *
    (table[0] + table[2]));
}

console.log(phi([76, 9, 4, 1]));

for (let entry of journal) {
    console.log(`${entry.events.length} events.`);
}

let primes = [2, 3, 7, 11, 13, 17, 23, 29, 31, "..."];

let primes2 = () => {
    return primes.slice(0, primes.indexOf(13)).
    concat(primes.slice(primes.indexOf(13) + 1));
};
let prime = primes2()
console.log(prime)

let insert = () => {
    let fisrtPortion = primes.slice(0, primes.indexOf(13)).sort();
    let secondPortion = primes.slice(primes.indexOf(13) + 1).sort().push(41)
    return fisrtPortion.concat(secondPortion);
};

let primesMutated = insert();
console.log(primesMutated)

console.log("Variadic functions ...")

function max(...numbers) {
    let result = -Infinity;
    for(let number of numbers) {
        if(number > result) result = number;
    }
    return result;
}

console.log(max(...[23, 43, 54,12, 546 ,76, 865, 231]))

function min(...numbers) {
    let result = Infinity;
    for(let number of numbers) {
        if(number < result) result = number;
    }
    return result;
}
let nums = [23,434,556,767,888,98,767]
console.log(min(...primes))

console.log(Math.random())

function randomPointOnCircle(radius) {
    let angle = Math.random() * 2 * Math.PI;
    return {
        x: Math.cos(angle),
        y: Math.sin(angle)
    };
}

console.log(randomPointOnCircle(14))

let jsonObject = {
    "squirrel": false,
    "events": ["work", "touched tree", "pizza", "running"]
    // no comments allowed on json
    //but this is just a mere object like any other
}

console.log(typeof jsonObject)

let js = JSON.stringify(jsonObject)
console.log(typeof js)

let string = JSON.stringify({squirrel: false,
    events: ["weekend"]});
    console.log(string);
    // → {"squirrel":false,"events":["weekend"]}
    console.log(JSON.parse(string).events);
    // → ["weekend"]

let json = JSON.parse(js).events[0];
 console.log(json)


function range(start, end, step = 1) {
    let nums = [];
    for(let n = start, index = 0;n <= end;n += step,index++) {
        nums[index] = n;
    }
    return nums;
}

function sum(...numbers) {
    let result = 0;
    for(let number of numbers) {
        result += number;
    }
    return result;
}

console.log(sum(...range(10, 1)))

primes = [0,1,2,3,4,5,6,7,8,9]

for(let pointer1 = 0, pointer2 = primes.length - 1;
    pointer1 < primes.length && pointer2 > 0;pointer1++,pointer2--) {
    let tmp = primes[pointer1];
    primes[pointer1] = primes[pointer2];
    primes[pointer2] = tmp
}

console.log(primes)
function repeat(n, action) {
	for (let index = 1; index <= n; index++) action(index);
}

repeat(8, console.log);

let labels = [];

repeat(4, (index) => labels.push(`Unit ${index}`));

console.log(labels);

function greaterThan(n) {
	console.log(`called outer with ${n}`);
	return (m) => {
		console.log(`called inner with ${m}`);
		return m > n ? m : n;
	};
}

// this variable is binded to function
//value returned by the function greaterthan(n)
//10 will be now taken as an argument to the inner function
// to be tested against m later
let greaterThan10 = greaterThan(10);

console.log(greaterThan10(12)); // this has to return true i.e 12 => 12 > 10

function noisy(func) {
	return (...args) => {
		console.log('calling with ', args);
		let result = func(...args);
		console.log('called with ', args, '. returned ', result);
		return result;
	};
}

let noise = noisy(Math.max);

console.log(noise(2, 4, 3, 5, 6, 1, 2, 3));

console.log(greaterThan(15)(13));

function unless(test, then, orThis) {
	if (test) {
		then();
	} else {
		orThis();
	}
}

var citizen = 'sheena';
var age = 16;
var citizenship = 'ugandan';
var hasNationalID = true;

// console.log(unless((age = 18, citizenship = 'kenyan') => {
//     return (age >= 18 && citizenship === 'ugandan')
// },() => {
//     console.log(`${citizen} is not eligible for voting! `)
// }));

console.log(
	unless(
		() => {
			return 6 % 2 === 0;
		},
		() => console.log('The number is odd!'),
		() => console.log('The number is Even!')
	)
);

function unless(test, then) {
	if (!test) then();
}

let units = [];
console.log(labels.forEach((element) => console.log(element, ' revised')));

labels.forEach((element) => units.push(element));
console.log(units);

let scriptDataSet = [
	{
		name: 'Coptic',
		ranges: [
			[994, 1008],
			[11392, 11508],
			[11513, 11520],
		],
		direction: 'ltr',
		year: -200,
		living: false,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
	{
		name: 'japanese',
		ranges: [
			[994, 1008],
			[11392, 11508],
			[11513, 11520],
		],
		direction: 'ltr',
		year: -200,
		living: true,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
	{
		name: 'latin',
		ranges: [
			[994, 1008],
			[11392, 11508],
			[11513, 11520],
		],
		direction: 'ltr',
		year: -200,
		living: true,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
	{
		name: 'greek',
		ranges: [
			[994, 1008],
			[11392, 11508],
			[11513, 11520],
		],
		direction: 'ltr',
		year: -200,
		living: true,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
	{
		name: 'Arabic',
		ranges: [
			[994, 1008],
			[11392, 11508],
			[11513, 11520],
		],
		direction: 'rtl',
		year: -200,
		living: true,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
	{
		name: 'hebrew',
		ranges: [
			[994, 1008],
			[11392, 11508],
			[11513, 11520],
		],
		direction: 'rtl',
		year: -200,
		living: false,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
	{
		name: 'Han',
		ranges: [
			[20938, 1008],
			[11392, 12508],
			[1513, 19520],
		],
		direction: 'rtl',
		year: -200,
		living: false,
		link: 'https://en.wikipedia.org/wiki/Coptic_alphabet',
	},
];

function filter(array, test) {
	let passed = [];
	for (let element of array) {
		if (test(element)) {
			passed.push(element);
		}
	}
	return passed;
}

let livingScripts = filter(scriptDataSet, (scriptDataSet) => {
	return scriptDataSet.living;
});

console.log(livingScripts);

console.log(
	scriptDataSet.filter((script) => {
		return script.direction === 'rtl';
	})
);

function mapp(array, transform) {
	let mapped = [];
	for (let element of array) {
		mapped.push(transform(element));
	}
	return mapped;
}

let rtlScripts = [];
rtlScripts = scriptDataSet.filter((script) => script.direction === 'rtl');

console.log(mapp(rtlScripts, (script) => script.name));
console.log(mapp(scriptDataSet, (script) => script.name));
console.log(
	scriptDataSet.map((script) => {
		return script.ranges;
	})
);
console.log('right to left scripts ', rtlScripts);

/**
 * @param {number[]} array to be folded or reduced
 * @param {{ (number: any, element: any): any; (arg0: number, arg1: any): number; }} combine a callback for every element of the array
 * @param {number}start an initialValue for the callback build on to return a reduction of the array
 */
function reduce(array, combine, start = 0) {
	let current = start;
	for (let element of array) {
		current = combine(current, element);
	}
	return current;
}

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

console.log(
	reduce(
		numbers,
		(number, element) => {
			return number + element;
		},
		10
	)
);
console.log(numbers.reduce((a, b) => a + b));

console.log(
	[2, 4, 5, 4, 5, 7, 8, 3, 2].reduce((a, b) => {
		return a * b;
	}, 1)
);

function countChars(script) {
	return script.ranges.reduce((count, [from, to]) => {
		return count + (to - from);
	}, 0);
}
console.log(
	scriptDataSet.reduce((a, b) => {
		console.log('counted');
		return countChars(a) > countChars(b) ? b : a;
	})
);

console.log(
	numbers.reduce((a, b) => {
		for (let number of numbers) {
			if (number === numbers[Math.round(numbers.length / 2)]) break;
		}
		return a + b;
	}, 0)
);

function characterScript(code) {
	for (let script of scriptDataSet) {
		if (
			script.ranges.some(([from, to]) => {
				return code >= from && code < to;
			})
		)
			return script;
	}
	return null;
}
let horse = '🐴';
console.log(characterScript(10000));

console.log(horse + 1);

console.log(horse.charCodeAt(0));

console.log(
	scriptDataSet.findIndex((script) => {
		console.log('the index should be ...');
		return script.name === 'hebrew' || script.name === 'latin';
	})
);

console.log(numbers);
let myName = 'Ian Balijawa';

for (let char of myName) {
	console.log(char);
}

console.log('trying tto flatten an array');

let nums = [
	[2, 9],
	[1, 5],
	[3, 3],
	[6, 6],
	[5, 4],
	[2, 5],
];

// function reduce(array, combine, start = 0) {
//   let current = start;
//   let flattened = [];
//   for(let element of array) {
//     for(let cell of a) {
//         flattened.push(combine(element, current))
//     }
//   }
//   return flattened;
// }

// console.log(reduce(nums, (a, b) => {
//   return a.concat(b);
// }, 1))

function getSentence({ subject, verb, object }) {
	return `${subject} ${verb} ${object}`;
}
const o = {
	subject: 'I',
	verb: 'love',
	object: 'JavaScript',
};

console.log(getSentence(o));
// setTimeout(() => console.log("helloworld"), 100);
// const o = {
//     name: 'Balijawa',
//     message: 'Hello, how was your morning ',
//     greetBackWards: () => {
//       let backWards = '';
//       for(let char of this.name) {
//         let index = 0, index2 = this.name.length - 1;
//         backWards[index] = this.name[index2];
//         index++;
//         index2--;
//       }
//       return backWards;
//     }
// };

//
// const ob = {
// name: 'Julie',
// greetBackwards: function() {
// function getReverseName() {
// let nameBackwards = '';
// for(let i=this.name.length - 1; i>=0; i--) {
// nameBackwards += this.name[i];
// }
// return nameBackwards;
// }
// return `${getReverseName()} si eman ym ,olleH`;
// },
// };

// console.log(ob.greetBackwards());

let func; //undefined function

{
	let o = { note: "this is safe here" };
	{
		func = function () {
			let countFunc = 0;
			console.log(`called ${countFunc++} times`);
		};
	}
}
func();
func();
func();

console.log("immediatlely invoked function expressions");

const message = (function () {
	let countFunc = 0;
	const secret = "This secret is between me and you";
	console.log(
		`The secret is ${
			secret.length
		} characters long! called ${countFunc++} times`
	);
})(); // this is an IIFE
message;
message;

const f = (function () {
	let count = 0;
	return function () {
		console.log(`I have been called ${++count} time(s).`);
	};
})();
f();
f();

let arr = "2,3,4,5,5,4,3,5,6,5";

arr = arr.split(",");

console.log(arr.push(7), arr);
console.log(arr.shift(), arr);
console.log(arr.pop(), arr);

const list = [
	{ name: "Suzanne" },
	{ name: "Jim" },
	{ name: "Trevor" },
	{ name: "Amanda" },
];

console.log(arr);

function normalize() {
	console.log(this.coords.reduce((n, m) => n * m) / 4);
}

normalize.call({ coords: [1, 4, 2, 3, 4], length: 4 });

let rabbit = { type: "white", line: "I'm the white rabbit" };

rabbit.speak = function (line) {
	console.log(line);
};

rabbit.speak("I'm jumping so high");

function jump() {
	return "jumping";
}
console.log(jump.call(rabbit));

function speak() {
	console.log(`The ${this.type} rabbit says ${this.line}`);
}

let blackRabbit = { type: "black", line: "I'm the black rabbit" };

speak.call(blackRabbit);

speak.call(rabbit);

let empty = {};

console.log(empty.toString);
console.log(empty.toString());

console.log(Object.getPrototypeOf({}));
console.log(function () {}.constructor === Function);

console.log(Object.getPrototypeOf(Object.__proto__));

console.log(Function.prototype);
console.log(Object.getPrototypeOf(() => {}));
console.log(Object.getPrototypeOf(() => {}) === Function.prototype);

console.log(Object.prototype === Object.getPrototypeOf(Object));

console.log(Object.getPrototypeOf(Object.prototype));

let protoRabbit = {
	speak(line) {
		console.log(`The ${this.type} rabbit says ${line}`);
	},
};

let killerRabbit = Object.create(protoRabbit);
killerRabbit.type = 'killer';
killerRabbit.speak("you're dead man");
protoRabbit.type = 'proto';
protoRabbit.speak('this line is for all of us');

function Rabbit(type) {
	this.type = type;
}
Rabbit.prototype.say = function (word) {
	console.log(`The ${this.type} rabbit says ${word}`);
};

Rabbit.prototype.speak = function (line) {
	console.log(`The ${this.type} rabbit says ${line}`);
};

let weirdRabbit = new Rabbit('weird');
let talkerRabbit = new Rabbit('talker');
weirdRabbit.type = 'weired';
weirdRabbit.speak('im wierd');
talkerRabbit.type = 'talker';

talkerRabbit.say('I talk alot!');
console.log(talkerRabbit.type);

console.log(Object.getPrototypeOf(Rabbit) === Function.prototype);
console.log(Object.getPrototypeOf(weirdRabbit) === Rabbit.prototype);

class Rabit {
	constructor(type) {
		this.type = type;
	}
	speak(line) {
		console.log(`The ${this.type} rabbit says ${line}`);
	}
	jump() {
		console.log(`The ${this.type} rabbit is jumping`);
	}
}

let jumpingRabbit = new Rabit('jumpingRabbit');
let blackRabbit = new Rabit('black');
blackRabbit.jump();

jumpingRabbit.speak('jumping is fun');

//an example of a class used as an expression i.e without declaration
//this is more specifically an immediatlely invoked class expression with the syntax
/**
 * new (class {
 *      functions /methods here ...
 * })();
 */
let object = new (class {
	getWord() {
		return 'Hello';
	}
})();

console.log(object.getWord());

Rabit.prototype.teeth = 'small'; //adding a new property in the object's prototype
//now the Rabit instance/object has a property teeth
Rabit.prototype.name = 'Ichuli';
Rabit.prototype.color = '';
jumpingRabbit.name = 'hare';
jumpingRabbit.color = 'white';
console.log(blackRabbit.teeth);
console.log(Rabit.prototype.teeth);
console.log(jumpingRabbit.name === blackRabbit.name);
console.log(jumpingRabbit.name, blackRabbit.name, Rabit.prototype.name);

// this next line shows that the toString method of the Array prototype overrides
// that of he object prototype so they are not equivalent
console.log(Array.prototype.toString === Object.prototype.toString);
console.log(typeof jumpingRabbit);

console.log([2, 4, 5, 5].join(','), jumpingRabbit.toString());
//using the toString method on an array does the same as using the join method with a comma , as a delimiter
console.log([1, 2, 3, 4, 5, 6].join(',') === [1, 2, 3, 4, 5, 6].toString());

//when you directly call the toString method from the Object prototype on the array, it produces a different string. it simply puts the word object and the name of the type between square brackets
console.log(Object.prototype.toString.call([12, 3, 4, 54, 32]));

//It's dangerous to use an object like this one below as a map coz it will returns properties that ask for but dont belong to it. such properties may belong to the object.prototype object
let ages = {
	Boris: 34,
	Julia: 22,
	Liang: 53,
};

console.log('toString' in ages); //it will be searched in the next prototype along the chain . this property will be found in the Object.prototype and thus will be returned
console.log(ages[toString]); //this should return something, even if undefined
console.log('ian' in ages); //no such property

//this is one safe way to create a map from plain objects. by passing null to the Object.create(). this doesnot create a prototype for this object

let heights = new Map();
heights = {
	Ian: 173,
	Sharif: 175,
	Collins: 177,
	charlotte: 161,
	Jjuuko: 169,
};

let schools = new Map();
// the methods has(), set() and get() are part of the map interface
schools.set('ian', { first: 'kihangire', second: 'mount' });
schools.set('sharif', { first: 'mount', second: 'namilyango' });
schools.set('collins', 'mengo');
console.log(schools.get('sharif'));
console.log(schools.has('collins'));
console.log(schools.get('ian').hasOwnProperty('first'));

console.log('toString' in schools); // very funny
console.log(schools.keys()); //we return only the properties belonging to that object's own self without traversing the prototype chain

// so let's try this one
console.log('toString' in schools.keys()); //even more very funny. this can be solved by using the method  hasOwnProperty()
console.log(schools.hasOwnProperty('toString')); // now not funny...now serious
// this looks for the 'toString' only among its own properties and no interfering with the next prototype in the chain

console.log(blackRabbit.toString()); //returns gibberish information

/**
 * @override
 *
 */
Rabit.prototype.toString = () => {
	return `a ${this.type} rabbit `;
};

blackRabbit.type = 'black';
console.log(blackRabbit.toString());

let sym = Symbol('name');
let sym2 = Symbol('figure');
console.log(sym === Symbol('name'));

let name = Symbol('name');

console.log(sym);

Rabit.prototype[sym] = 100;
console.log(blackRabbit.sym);

let list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

let iter = list[Symbol.iterator]();
for (let value of list) {
	console.log(iter.next());
}

while (!iter.next().done) {
	console.log(iter.next()[value]);
}

const toStringSymbol = Symbol('toString');
Array.prototype[toStringSymbol] = function () {
	return `${this.length} cm of yarn`;
};

console.log([1, 2, 4, 4, 5, 6].toString());
console.log([1, 2, 4, 4, 5, 6][toStringSymbol]());

let okIterator = 'OK'[Symbol.iterator]();
console.log(okIterator.next());
console.log(okIterator.next());
console.log(okIterator.next().done);

class Matrix {
	constructor(width, height, element = (x, y) => undefined) {
		this.width = width;
		this.height = height;
		this.content = [];
		for (let y = 0; y < height; y++) {
			for (let x = 0; x < width; x++) {
				this.content[y * width + x] = element(x, y);
			}
		}
	}
	get(x, y) {
		return this.content[y * this.width + x];
	}
	set(x, y, value) {
		this.content[y * this.width + x] = value;
	}
}

class MatrixIterator {
	constructor(matrix) {
		this.x = 0;
		this.y = 0;
		this.matrix = matrix;
	}
	next() {
		if (this.y == this.matrix.height) return { done: true };
		let value = {
			x: this.x,
			y: this.y,
			value: this.matrix.get(this.x, this.y),
		};
		this.x++;
		if (this.x == this.matrix.width) {
			this.x = 0;
			this.y++;
		}
		return { value, done: false };
	}
}

Matrix.prototype[Symbol.iterator] = function () {
	return new MatrixIterator(this);
};

let matrix = new Matrix(2, 2, (x, y) => `value ${x},${y}`);
for (let { x, y, value } of matrix) {
	console.log(x, y, value);
}
// → 0 0 value 0,0
// → 1 0 value 1,0
// → 0 1 value 0,1
// → 1 1 value 1,1

console.log(schools.size);

class Temperature {
	constructor(celcius) {
		this.celcius = celcius;
	}

	get fahrenheit() {
		return this.celcius * 1.8 + 32;
	}
	set fahrenheit(value) {
		this.celcius = (value - 32) / 1.8;
	}
	static fromFahrenheit(value) {
		return new Temperature((value - 32) / 1.8);
	}
}

let temp = new Temperature(22);
temp.fahrenheit = 212;
console.log(temp.celcius);

console.log(Temperature.fromFahrenheit(200));

class SymmetricMatrix extends Matrix {
	constructor(size, element = (x, y) => undefined) {
		super(size, size, (x, y) => {
			if (x < y) return element(y, x);
			else return element(x, y);
		});
	}

	/**
	 * @overrride
	 */
	set(x, y, value) {
		super.set(x, y, value);
		if (x != y) {
			super.set(y, x, value);
		}
	}
}

let matrix2 = new SymmetricMatrix(5, (x, y) => {
	return `${x} ${y}`;
});

console.log(matrix2.get(4, 3));

console.log(new SymmetricMatrix() instanceof Matrix);
console.log(new SymmetricMatrix(3, (x, y) => `${x}, ${y}`) instanceof Object);
console.log([] instanceof Array === 'ian' instanceof String); //??
// works only for non-primitive datatypes i.e objects and classes

let me = new String('ian');
console.log(me.valueOf());

// var arr = [2,4,6,8,4,60,80,74];
const arr = [
	{ name: 'Suzanne' },
	{ name: 'Jim' },
	{ name: 'Trevor' },
	{ name: 'Amanda' },
];
console.log(arr.sort()); // arr unchanged
console.log(arr.sort((a, b) => (a.name > b.name ? 1 : -1))); // arr sorted alphabetically
// by name property
console.log(arr.sort((a, b) => (a.name[1] < b.name[1] ? 1 : -1)));

for (let element in arr) {
	console.log(arr[element]);
}

const SYM = Symbol();

const o = { a: 1, b: 2, c: 3, [SYM]: 4 };

for (let prop in o) {
	if (!o.hasOwnProperty(prop)) continue;
	console.log(`${prop}: ${o[prop]}`);
}

for (const key in o) {
	if (o.hasOwnProperty.call(o, key)) {
		const element = o[key];
		console.log(element);
	}
}

Object.keys(o).forEach((element) => console.log(`${element}: ${o[element]}`));

const obj = { apple: 1, xochitl: 2, balloon: 3, guitar: 4, xylophone: 5 };

//this is more handy than the for ... in loop when iterating over properties. you can perform more array-related actions with it coz it returns an array. ie filter() and match <- regex
Object.keys(obj)
	.filter((prop) => prop.match(/^x/)) //filtering out properties starting with the letter x
	.forEach((prop) => console.log(`${prop}: ${obj[prop]}`));

// class Car {
// 	constructor(make, model) {
// 		this.make = make;
// 		this.model = model;
// 		this.__userGears = ['P', 'N', 'R', 'D'];
// 		this.__userGear = this.__userGears[0];
// 	}
// 	get userGear() {
// 		return this.__userGear;
// 	}
// 	set userGear(gear) {
// 		if (
// 			this.__userGears.indexOf(gear) < 0 ||
// 			this.__userGears.indexOf(gear) > this.__userGears.length - 1
// 		) {
// 			throw new Error(`Invalid gear ${gear}`);
// 		}
// 		this.__userGear = gear;
// 	}

// 	shift(gear) {
// 		this.__userGear = gear;
// 	}
// }

// const car1 = new Car('tesla', 'x09P');
// const car2 = new Car();

// console.log(car1.userGear)

//poor man's access restriction

const Car = (function () {
	const carProps = new WeakMap();

	class Car {
		/**
		 * @param {any} make
		 * @param {any} model
		 */
		constructor(make, model) {
			this.make = make;
			this.model = model;
			this.__userGears = ['P', 'N', 'R', 'D'];
			this.__userGear = this.__userGears[0];
			carProps.set(this, { userGear: this.__userGears[0] });
		}
		get userGear() {
			return carProps.get(this).userGear;
		}
		set userGear(gear) {
			if (
				this.__userGears.indexOf(gear) < 0 ||
				this.__userGears.indexOf(gear) > this.__userGears.length - 1
			) {
				throw new Error(`Invalid Gear ${gear}`);
			}
			carProps.get(this).userGear = gear;
		}
		/**
		 * @param {string} gear
		 */
		shift(gear) {
			this.userGear = gear;
		}
		/**
		 * @override
		 */
		toString() {
			// @ts-ignore
			return `${this.make} ${this.model}: ${this.vin}`;
		}
	}
	return Car;
})();

let car1 = new Car('tesla', 'oxPIE*#&');
console.log(car1.shift);
car1.shift =
	/**
	 * @param {string} gear
	 */
	function (gear) {
		this.userGear = gear.toUpperCase();
	};

car1.shift('d');
console.log(Object.getPrototypeOf(Car));
console.log(car1.userGear);

class Super {
	constructor() {
		this.name = 'Super';
		this.isSuper = true;
	}
}
// this is valid, but not a good idea...
Super.prototype.sneaky = 'not recommended!';
class Sub extends Super {
	constructor() {
		super();
		this.name = 'Sub';
		this.isSub = true;
	}
}
const ob = new Sub();
for (let p in ob) {
	console.log(
		`${p}: ${ob[p]}` + (ob.hasOwnProperty(p) ? '' : ' (inherited)')
	);
}

console.log(Object.keys(ob));
console.log(car1.toString());

class InsurancePolicy {}
/**
 * @param {object} o
 */
function makeInsurable(o) {
    //this is the mixin between the car class and the InsurancePolicy class
    /**
     * 
     * @param {InsurancePolicy} p 
     */
	o.addInsurancePolicy = function (p) {
		this.insurancePolicy = p;
	};
	o.getInsurancePolicy = function () {
		return this.insurancePolicy;
	};
	o.isInsured = function () {
		return !!this.insurancePolicy;
	};
}

const car3 = new Car();
makeInsurable(car3);
// @ts-ignore
car3.addInsurancePolicy(new InsurancePolicy());

const u1 = { name: 'Cynthia' };
const u2 = { name: 'Jackson' };
const u3 = { name: 'Olive' };
const u4 = { name: 'James' };

const userRoles = new Map();

userRoles.set(u1, 'user').set(u2, 'user').set(u3, 'Admin');

// console.log(userRoles);

const userRoles2 = new Map([
	[u1, 'user'],
	[u2, 'user'],
	[u3, 'Admin'],
	[u4, 'Analyst'],
]);

// console.log(userRoles2.get(u3));

// console.log(userRoles2.has(u4));
// console.log(userRoles2.size);
// console.log(userRoles2.values());

// console.log(userRoles2.keys())

const SecretHolder = (function () {
	const secrets = new WeakMap();
	// @ts-ignore
	export class SecretHold {
		set setSecret(secret) {
			secrets.set(this, secret);
		}
		set setType(type) {
			secrets.set(this, type);
		}
		get getSecret() {
			return secrets.get(this);
        }
        get getType() {
            return secrets.get(this)
        }
    };
    return SecretHold;
})();

const a = new SecretHolder();
const b = new SecretHolder();

const holder = (function () {
	return class extends SecretHolder {
		constructor() {
			super();
			console.log('holder created');
		}
	};
})();

const c = new holder();
c.setSecret = 'secret';
c.setType = 'love';
console.log(c.getSecret)
console.log(c.getType);


const roles = new Set()
roles.add('user')
console.log(roles)
const email = 'ianbalijawa'; //whoops

try {
	const validatedEmail = validateEmail(email);
	if (validatedEmail instanceof Error) {
		console.error(`Error: ${validatedEmail.message}`);
		throw new Error(`invalid email address: ${validatedEmail.message}`);
	}
	console.log(`Valid email: ${validatedEmail}`);
} catch (e) {
	console.log(`Error: ${e.message}`);
}

function validateEmail(email) {
	return email.match(/@/) ? email : new Error(`Invalid email : ${email}`);
}

function a_() {
	console.log('a: calling b');
	b_();
	console.log('a finished.');
}

function b_() {
	console.log('b: calling c');
	c_();
	console.log('b is now done.');
}

function c_() {
	console.log('c: calling d');
	throw new Error('Error thrown by c');
	console.log('c is done.'); //unreachable code
}

function d_() {
	console.log('d: calling c');
	c_();
	console.log('d: done');
}
try {
    a_();
} catch (error) {
    console.log(error.stack)
}


console.log('continue with program execution');

class Matrix {
    constructor(width, height, element = (x, y) => undefined) {
      this.width = width;
      this.height = height;
      this.content = [];
      for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
          this.content[y * width + x] = element(x, y);
        }
      }
    }
    get(x, y) {
      return this.content[y * this.width + x];
    }
    set(x, y, value) {
      this.content[y * this.width + x] = value;
    }
  }
  
  class MatrixIterator {
    constructor(matrix) {
      this.x = 0;
      this.y = 0;
      this.matrix = matrix;
    }
    next() {
      if (this.y == this.matrix.height) return { done: true };
      let value = {
        x: this.x,
        y: this.y,
        value: this.matrix.get(this.x, this.y),
      };
  
      this.x++;
      if (this.x == this.matrix.width) {
        this.x = 0;
        this.y++;
      }
      return { value, done: false };
    }
  }
  
  Matrix.prototype[Symbol.iterator] = function () {
    return new MatrixIterator(this);
  };
  
  let matrix = new Matrix(2, 2, (x, y) => `value ${x},${y}`);
  for (let { x, y, value } of matrix) {
    console.log(x, y, value);
  }
  // → 0 0 value 0,0
  // → 1 0 value 1,0
  // → 0 1 value 0,1
  // → 1 1 value 1,1
  
  let varyingSize = {
    get size() {
      return Math.floor(Math.random() * 100);
    },
  };
  console.log(varyingSize.size);
  // → 73
  console.log(varyingSize.size);
  // → 49
  
  class Temperature {
    constructor(celsius) {
      this.celsius = celsius;
    }
    get fahrenheit() {
      return this.celsius * 1.8 + 32;
    }
    set fahrenheit(value) {
      this.celsius = (value - 32) / 1.8;
    }
    static fromFahrenheit(value) {
      return new Temperature((value - 32) / 1.8);
    }
  }
  let temp = new Temperature(22);
  console.log(temp.fahrenheit);
  // → 71.6
  temp.fahrenheit = 86;
  console.log(temp.celsius);
  // → 30
  
  class SymmetricMatrix extends Matrix {
    constructor(size, element = (x, y) => undefined) {
      super(size, size, (x, y) => {
        if (x < y) return element(y, x);
        else return element(x, y);
      });
    }
    set(x, y, value) {
      super.set(x, y, value);
      if (x != y) {
        super.set(y, x, value);
      }
    }
  }
  
  // let matrix = new SymmetricMatrix(5, (x, y) => `${x},${y}`);
  // console.log(matrix.get(2, 3));
  // // → 3,2
  